#include <stdio.h>

int main(int argc, char const *argv[])
{
    float lado1;
    float lado2;
    float lado3;
    float perimetro;

    printf("Este programa sirve para calcular el perimtro de un triángulo escaleno");
    printf("\nIngresa el valor del lado 1 del tríangulo escaleno: ");
    scanf("%f", &lado1);

    while(lado1 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado1);
    }

    printf("Ingresa el valor del lado 2 del tríangulo escaleno: ");
    scanf("%f", &lado2);

    while(lado2 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado2);
    }

    printf("Ingresa el valor del lado 3 del tríangulo escaleno: ");
    scanf("%f", &lado3);

    while(lado3 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado3);
    }

    perimetro = lado1 + lado2 + lado3;
    printf("El perimetro del triángulo es %g", perimetro);
    return 0;
}
