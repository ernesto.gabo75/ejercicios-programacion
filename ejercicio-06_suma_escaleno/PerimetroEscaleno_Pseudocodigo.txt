Inicio
    Variable lado1 es Numerico Real
    Variable lado2 es Numerico Real
    Variable lado3 es Numerico Real
    Variable perimetro es Numerico Real

    Escribir "Este programa sirve para calcular el perimtro de un triángulo escaleno"
    Escribir "Ingresa el valor del lado 1 del tríangulo escaleno: "
    Leer lado1

    while lado1 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado1
    end-while

    Escribir "Ingresa el valor del lado 2 del tríangulo escaleno: "
    Leer lado2

    while lado2 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado2
    end-while

    Escribir "Ingresa el valor del lado 3 del tríangulo escaleno: "
    Leer lado3

    while lado3 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado3
    end-while

    perimetro = lado1 + lado2 + lado3

    Escribir "El perimetro del triángulo es ", perimetro
Fin