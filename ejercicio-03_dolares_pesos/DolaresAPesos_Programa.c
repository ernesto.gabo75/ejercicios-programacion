#include <stdio.h>

#define VALOR_PESOS 20.04

int main(int argc, char const *argv[])
{
    float montoEnDolares;
    float montoEnPesos;

    printf("Este programa sirve para convertir" 
        " un monto en dolares a su equivalente en pesos");
    printf("\nIngresa el monto que deseas convertir: ");
    scanf("%f", &montoEnDolares);

    while(montoEnDolares <= 0)
    {
        printf("El monto debe ser un valor valido, ingresa otro: ");
        scanf("%f", &montoEnDolares);
    }

    montoEnPesos = montoEnDolares * VALOR_PESOS;
    printf("La conversión de %g dolares a pesos es %g", montoEnDolares, montoEnPesos);
    return 0;
}
