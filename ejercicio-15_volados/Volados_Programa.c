#include <stdio.h>
#include <stdlib.h>

#define MAX_RONDAS 3

int main(int argc, char const *argv[])
{
    int ronda;
    int bolsaCPU;
    int bolsaJugador;
    int victoriasJugador;
    int victoriasCPU;
    victoriasJugador = victoriasCPU = 0;
    bolsaCPU = bolsaJugador = 500;
    int tiro;
    int respuesta;
    char respuestaStr[2];
    int apuestaJugador;
    int apuestaCPU;

    printf("Este programa sirve para jugar volados\n");

    for (ronda =1; ronda <= MAX_RONDAS; ronda++)
    {
        printf("Ingresa tu apuesta(min. 20, max. 100): ");
        scanf("%d", &apuestaJugador);

        while(apuestaJugador < 20 || apuestaJugador > 100){
            printf("Ingresa una apuesta valida(min.20, max. 100): ");
            scanf("%d", &apuestaJugador);
        }
        
        apuestaCPU = rand() % 80 + 20;
        printf("La apuesta del CPU es de %d\n", apuestaCPU);
        printf("¿El siguiente tiro sera? 1. Aguila 2. Sol: ");
        scanf("%d", &respuesta);
        while(respuesta != 1 && respuesta != 2){
            printf("Ingresa una respuesta valida(1. Aguila 2. Sol): ");
            scanf("%d", &respuesta);
        }

        tiro = rand() % 2 + 1;

        if(tiro == 1){
            printf("El tiro fue Aguila\n");
        }else{
            printf("El tiro fue Sol\n");
        }

        printf("\n-------------------------------------------\n");

        if(tiro == respuesta){
            bolsaJugador += apuestaJugador;
            bolsaCPU -= apuestaJugador;
            victoriasJugador++;
        }else{
            bolsaCPU += apuestaJugador;
            bolsaJugador -= apuestaJugador;
            victoriasCPU++;
        }
    }

    printf("Bolsa Jugador: %d\n", bolsaJugador);
    printf("Bolsa CPU: %d\n", bolsaCPU);
    

    if(victoriasJugador > victoriasCPU){
        printf("GANASTE!!!");
    }else{
        printf("PERDISTE, gano el CPU");
    }
    return 0;
}
