#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NUM 30

float calcularPerimetroEscaleno();
float calcularPerimetroEquilatero();
float calcularPerimetroIsoceles();

int main(int argc, char const *argv[])
{
    int opcion;
    float perimetro;
    char mensajePerimetro[100];
    char num[MAX_NUM];
    printf("Ingresa el tipo de triángulo que quieres calcular: \n1. Equilatero \n2. Isoceles \n3. Escaleno\n");
    scanf("%d", &opcion);

    while(opcion < 1 || opcion > 3)
    {
        printf("Ingresa una opción valida \n1. Equilatero \n2. Isoceles \n3. Escaleno\n");
        scanf("%d", &opcion);
    }

    switch (opcion)
    {
        case 1:
            perimetro = calcularPerimetroEquilatero();
            strcpy(mensajePerimetro, "El perimetro del triangulo equilatero es ");
            sprintf(num, "%g", perimetro);
            strcat(mensajePerimetro, num);
            break;
        case 2:
            perimetro = calcularPerimetroIsoceles();
            strcpy(mensajePerimetro, "El perimetro del triangulo isoceles es ");
            sprintf(num, "%g", perimetro);
            strcat(mensajePerimetro, num);
            break;
        case 3:
            perimetro = calcularPerimetroEscaleno();
            strcpy(mensajePerimetro, "El perimetro del triangulo escaleno es ");
            sprintf(num, "%g", perimetro);
            strcat(mensajePerimetro, num);
            break;
        default:
             strcpy(mensajePerimetro, "ERROR!!!");
            break;
    }

    printf("%s", mensajePerimetro);
    return 0;
}

float calcularPerimetroEquilatero()
{
    float tamanioLado;
    float perimetro;

    printf("\nIngresa el valor de un lado del triángulo equilatero: ");
    scanf("%f", &tamanioLado);

    while (tamanioLado <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioLado);
    }

    perimetro = tamanioLado * 3;
    return perimetro;
}

float calcularPerimetroIsoceles()
{
    float tamanioLado;
    float tamanioBase;
    float perimetro;

    printf("\nIngresa el valor de la base del triángulo isoceles: ");
    scanf("%f", &tamanioBase);
    while(tamanioBase <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioBase);
    }

    printf("\nIngresa el valor de un lado del triángulo isoceles: ");
    scanf("%f", &tamanioLado);

    while (tamanioLado <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioLado);
    }

    perimetro = tamanioBase + tamanioLado * 2;
    return perimetro;
}

float calcularPerimetroEscaleno()
{
    float lado1;
    float lado2;
    float lado3;
    float perimetro;

    printf("\nIngresa el valor del lado 1 del tríangulo escaleno: ");
    scanf("%f", &lado1);

    while(lado1 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado1);
    }

    printf("Ingresa el valor del lado 2 del tríangulo escaleno: ");
    scanf("%f", &lado2);

    while(lado2 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado2);
    }

    printf("Ingresa el valor del lado 3 del tríangulo escaleno: ");
    scanf("%f", &lado3);

    while(lado3 <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &lado3);
    }

    perimetro = lado1 + lado2 + lado3;
    return perimetro;
}