Funcion float perimetroEquilatero()
Inicio
    Variable tamanioLado es Numerico Real
    Variable perimetro es Numerico Real

    Escribir "Ingresa el valor de un lado del triángulo equilatero"

    leer tamanioLado
    while tamanioLado <= 0
        Escribir "Ingresa un valor positivo"
        Leer tamanioLado
    end-while

    perimetro = tamanioLado * 3;
    retorna perimetro
Fin

Funcion float perimetroEscaleno()
Inicio 
    Variable lado1 es Numerico Real
    Variable lado2 es Numerico Real
    Variable lado3 es Numerico Real
    Variable perimetro es Numerico Real

    Escribir "Ingresa el valor del lado 1 del tríangulo escaleno: "
    Leer lado1

    while lado1 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado1
    end-while

    Escribir "Ingresa el valor del lado 2 del tríangulo escaleno: "
    Leer lado2

    while lado2 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado2
    end-while

    Escribir "Ingresa el valor del lado 3 del tríangulo escaleno: "
    Leer lado3

    while lado3 <= 0
        Escribir "Ingresa un valor positivo: "
        Leer lado3
    end-while

    perimetro = lado1 + lado2 + lado3

    retorna perimetro
Fin

Funcion float perimetroIsoceles()
Inicio
    Variable tamanioBase es Numerico Real
    Variable tamanioLado es Numerico Real
    Variable perimetro es Numerico Real

    Escribir "Ingresa el valor de la base del triángulo isoceles"

    Leer tamanioBase
    while tamanioBase <= 0
        Escribir "Ingresa un valor positivo"
        Leer tamanioBase
    end-while

    Escribir "Ingresa el valor de un lado del triángulo isoceles"

    Leer tamanioLado
    while tamanioLado <= 0
        Escribir "Ingresa un valor positivo"
        Leer tamanioLado
    end-while

    perimetro = tamanioBase + 2 * tamanioLado;
    retorna perimetro
Fin

Inicio
    Variable opcion es Numerico Entero
    Variable perimetro es Numerico Real
    Variable mensajePerimetro es una Cadena de Caracteres
    Escribir "Este programa sirve para calcular el perimetro de cualquier tipo de triángulo"
    Escribir "Ingresa el tipo de triángulo que quieres calcular: 1. Equilatero 2. Isoceles 3. Escaleno"

    Leer opcion 
    while opcion < 1 AND opcion > 3
        Escribir "Ingresa una opcion valida: "
        Leer opcion
    end-while

    switch
        case 1:
            perimetro = perimetroEquilatero()
            mensajePerimetro = "El perimetro del triángulo equilatero es ", perimetro
            break;
        case 2:
            perimetro = perimetroIsoceles()
            mensajePerimetro = "El perimetro del triángulo isoceles es ", perimetro
            break;
        case 3:
            perimetro = perimetroEscaleno()
            mensajePerimetro = "El perimetro del triángulo escaleno es ", perimetro
            break;
        default:
            perimetro = 0
    end-switch  

    Escribir mensajePerimetro
Fin