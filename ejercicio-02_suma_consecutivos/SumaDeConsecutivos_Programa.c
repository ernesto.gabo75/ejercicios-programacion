#include <stdio.h>

int main(int argc, char const *argv[])
{
    int tope;
    int i = 1;
    int suma = 0;

    printf("Este programa sirve para mostrar la suma consecutiva de 1 dado hasta un número ingresado\n");
    printf("Ingresa un número entre 1-50: ");
    scanf("%d", &tope);

    while(tope < 1 || tope > 50)
    {
        printf("El número debe estar entre 1-50, ingresa otro: ");
        scanf("%d", &tope);
    }

    for (i; i <= tope; i++)
    {
        suma += i;
    }

    printf("El resultado de la suma es: %d", suma);
    
    return 0;
}
