Inicio
    Variable tope es Numerico Entero
    Variable i es Numerico Entero
    Variable suma es Numerico Entero

    Escribir "Este programa sirve para mostrar 
        la suma consecutiva de número dado hasta 50\n"
    Escribir "Escribe un número entre 1-50: "
    Leer tope

    while tope < 1 OR tope > 50
        Escribir "El número debe estar entre 1-50, ingresa otro número: "
        Leer tope
    end-while

    suma = 0
    for(i = 1; i <= tope; i++)
        suma = suma + i
    end-for

    Escribir "El resultado de la suma es:", suma
Fin