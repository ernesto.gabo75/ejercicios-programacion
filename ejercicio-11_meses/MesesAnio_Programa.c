#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    char numeroMesStr[2];
    char *nombreMes;
    char *end;
    printf("Ingresa el número del mes: ");
    fgets(numeroMesStr, sizeof(numeroMesStr) + 1, stdin);

    
    long numeroMes = strtol(numeroMesStr, &end, 10);
    while(end == numeroMesStr)
    {
        printf("Ingresa un número valido: ");
        fgets(numeroMesStr, sizeof(numeroMesStr) + 1, stdin);
        numeroMes = strtol(numeroMesStr, &end, 10);
    }
    switch (numeroMes)
    {
        case 0:
            nombreMes = "enero";
            break;
        case 1:
            nombreMes = "febrero";
            break;
        case 2:
            nombreMes = "marzo";
            break;
        case 3:
            nombreMes = "abril";
            break;
        case 4:
            nombreMes = "mayo";
            break;
        case 5:
            nombreMes = "junio";
            break;
        case 6:
            nombreMes = "julio";
            break;
        case 7:
            nombreMes = "agosto";
            break;
        case 8:
            nombreMes = "septiembre";
            break;
        case 9:
            nombreMes = "octubre";
            break;
        case 10:
            nombreMes = "noviembre";
            break;
        case 11:
            nombreMes = "diciembre";
            break;
        default:
            nombreMes = "No existe ese mes";
            break;
    }

    printf("%s", nombreMes);
    return 0;
}
