#include <stdio.h>

int main(int argc, char const *argv[])
{
    float tamanioLado;
    float tamanioBase;
    float perimetro;

    printf("Este programa sirve para calcular el perimetro de un triángulo isoceles");
    printf("\nIngresa el valor de la base del triángulo isoceles: ");
    scanf("%f", &tamanioBase);
    while(tamanioBase <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioBase);
    }

    printf("\nIngresa el valor de un lado del triángulo isoceles: ");
    scanf("%f", &tamanioLado);

    while (tamanioLado <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioLado);
    }

    perimetro = tamanioBase + tamanioLado * 2;
    printf("El perimetro del triángulo es %g", perimetro);
    
    return 0;
}
