#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_SEG 3

//void bubbleSort(int *array, int sizeArray);
//void intercambio(int *a, int *b);

int main(int argc, char const *argv[])
{
    int i;
    int segmentos[MAX_SEG];
    const char *mensaje[]= {"segmento 1", "segmento 2", "segmento 3"};
    printf("Este programa sirve para determinar si 3 segmentos pueden ser un triángulo\n");
    for (i = 0; i < MAX_SEG; i++){
        printf("Ingresa el valor de %s: ", mensaje[i]);
        scanf("%d", &segmentos[i]);

        if(segmentos[i] <= 0){
            printf("No puede ser un triángulo debido al segmento negativo");
            exit(0);
        }
    }

    //bubbleSort(segmentos, MAX_SEG);

    if(segmentos[0] + segmentos[1] <= segmentos[2]){
        printf("No puede ser un triángulo debido al segmento con valor %d", segmentos[2]);
        exit(0);
    }

    if(segmentos[0] + segmentos[2] <= segmentos[1]){
        printf("No puede ser un triángulo debido al segmento con valor %d", segmentos[1]);
        exit(0);
    }

    if(segmentos[1] + segmentos[2] <= segmentos[0]){
        printf("No puede ser un triángulo debido al segmento con valor %d", segmentos[0]);
        exit(0);
    }

    printf("Los segmentos cumplen con las desigualdades, por lo que puede ser un triángulo");
    
    return 0;
}

/* void bubbleSort(int *array, int sizeArray){
    int i;
    bool swap;
    int iter;
    while(swap == true){
        swap = false;
        for (i = 0; i < sizeArray - 1 - iter; i++){
            if(array[i] > array[i + 1]){
                intercambio(&array[i], &array[i + 1]);
                swap = true;
            }
        }
        iter += 1;
    }
}

void intercambio(int *a, int *b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
} */