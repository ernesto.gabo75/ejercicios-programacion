#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define MAX_FECHA 24
#define MAX_PALABRAS_FECHA 5
#define MAX_MESES 12


char *toUpper(const char *mes);
int comprobarMes(const char *mes);
int obtenerDiasMes(int numeroMes, bool bisiesto);
bool comprobarAnioBisiesto(int anio);
int convertirStrtoInt(const char *str);
bool fechaReal(char *fechaCandidata);

int main(int argc, char const *argv[])
{
    char fechaCandidata[MAX_FECHA];
    
    printf("Este programa sirve para determinar si una fecha es real\n");
    printf("Ingresa la fecha: ");
    fgets(fechaCandidata, MAX_FECHA, stdin);

    if(fechaReal(fechaCandidata)){
        printf("La fecha introducida es correcta!!");
    }else{
        printf("La fecha introducida es incorrecta");
    }


    return 0;
}

bool fechaReal(char *fechaCandidata){
    int i;
    const char *delim = " ";
    char *fechaSeparada[MAX_PALABRAS_FECHA];
    fechaSeparada[0] = strtok(fechaCandidata, delim);

    
    for(i = 1; i < MAX_PALABRAS_FECHA; i++){
        fechaSeparada[i] = strtok(NULL, delim);
    }
    
    
    int mesNum = comprobarMes(fechaSeparada[2]);
    if(mesNum == -1){
        return false;
    }

    int anio;
    bool bisiesto = false;
    if(fechaSeparada[4] != NULL){
        anio = convertirStrtoInt(fechaSeparada[4]);
        if(anio == -1){
            return false;
        }
        bisiesto = comprobarAnioBisiesto(anio);
        
    }   

    int diaTope = obtenerDiasMes(mesNum, bisiesto);
    int dia = convertirStrtoInt(fechaSeparada[0]);
    if(dia == -1){
        return false;
    } else if(dia > diaTope || dia < 0){
        return false;
    } else{
        return true;
    }   
}

char *toUpper(const char *mes){
    int i = 0;
    char *mesUpper = (char *) malloc(sizeof(mes));

    while(mes[i] != '\0'){
        if(mes[i] != '\n'){
            mesUpper[i] = (char) toupper((int)mes[i]);
        }
        i++;
    }
    return mesUpper;
}

int comprobarMes(const char *mes){
    int i;
    char *mesUpper = toUpper(mes);
    const char *meses[] = {"ENERO", "FEBRERO", "MARZO",
                            "ABRIL", "MAYO", "JUNIO",
                            "JULIO", "AGOSTO", "SEPTIEMBRE",
                            "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};
    for (i = 0; i < MAX_MESES; i++){
        if(strcmp(mesUpper, meses[i]) == 0){
            return i + 1;
        }
    }
    return -1;
}

int obtenerDiasMes(int numeroMes, bool bisiesto){
    switch (numeroMes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
            break;
        case 2:
            if(bisiesto){
                return 29;
            }else{
                return 28;
            }
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
            break;
        default:
            return -1;
            break;
    }
}

bool comprobarAnioBisiesto(int anio){
    if(anio % 4 == 0 && anio % 100 == 0 && anio % 400 == 0){
        return true;
    }
    else{
        return false;
    }
}

int convertirStrtoInt(const char *str){
    char *end;
    int num = (int) strtol(str, &end, 10);
    if(str == end || num > 2022 || num < 0){
        return -1;
    }

    return num;
}
