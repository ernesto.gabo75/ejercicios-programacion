#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{
    srand(time(NULL));
    int numeroAleatorio = rand() % 100 + 1;
    int intento;
    int respuesta;
    char respuestaStr[3];
    char *end;

    printf("Este programa sirve para jugar \"adivina el numero\"\n");

    for (intento = 1; intento <= 5; intento++)
    {
        printf("¿Puedes adivinar el numero que estoy pensando(1-100)? ");
        fgets(respuestaStr, sizeof(respuestaStr)+1, stdin);
        respuesta = strtol(respuestaStr, &end, 10);
        fflush(stdin);

        while(end == respuestaStr || respuesta > 100 || respuesta < 1){
            printf("\nIngresa una respuesta valida: ");
            fgets(respuestaStr, 4, stdin);
            respuesta = strtol(respuestaStr, &end, 10);
            fflush(stdin); // Se limpia el buffer porque en caso de meter más de 4 caracteres se guarda la entrada y se coloca
                            // iteracion, por lo que itera dos veces
        }

        if(respuesta == numeroAleatorio){
            printf("GANASTE, como supiste que era %d", numeroAleatorio);
            return 0;
        }
    }

    printf("PERDISTE, el numero que estaba pensando era %d", numeroAleatorio);
    
    return 0;
}
