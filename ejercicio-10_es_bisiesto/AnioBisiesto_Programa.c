#include <stdio.h>
#include <stdlib.h>

#define MAX_ANIO 5

int main(int argc, char const *argv[])
{
    char candidatoAnioStr[MAX_ANIO];
    long candidatoAnio;

    printf("%u", sizeof(candidatoAnioStr));
    printf("Ingresa el anio: ");
    fgets(candidatoAnioStr, sizeof(candidatoAnioStr), stdin);

    candidatoAnio = atoi(candidatoAnioStr);
    while(candidatoAnio <= 0)
    {
        printf("Por favor, ingresa un anio valido: ");
        fgets(candidatoAnioStr, sizeof(candidatoAnioStr), stdin);
        candidatoAnio = atol(candidatoAnioStr);
    }

    if(candidatoAnio % 4 == 0 && candidatoAnio % 100 == 0 && candidatoAnio % 400 == 0){
        printf("El anio %d si es biesiesto", candidatoAnio);
    }
    else{
        printf("El anio %d no es bisiesto", candidatoAnio);
    }


    return 0;
}
