#include <stdio.h>

int main(int argc, char const *argv[])
{
    float tamanioLado;
    float perimetro;

    printf("Este programa sirve para calcular el perimetro de un triángulo equilatero");
    printf("\nIngresa el valor de un lado del triángulo equilatero: ");
    scanf("%f", &tamanioLado);

    while (tamanioLado <= 0)
    {
        printf("Ingresa un valor positivo: ");
        scanf("%f", &tamanioLado);
    }

    perimetro = tamanioLado * 3;
    printf("El perimetro del triángulo es %g", perimetro);
    
    return 0;
}
