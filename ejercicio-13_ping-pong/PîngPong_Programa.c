#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Este programa muestra del 1-100 y escribe ping-pong\n");
    int i = 1;

    for (i; i <= 100; i++)
    {
        if(i % 5 == 0 && i % 3 == 0){
            printf("ping-pong");
        }else if(i % 3 == 0){
            printf("ping");
        }else if(i % 5 ==0){
            printf("pong");
        }else{
            printf("%d", i);
        }
        printf("\n");
    }
    
    return 0;
}
