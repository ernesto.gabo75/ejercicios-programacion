#include <stdio.h>

int main(int argc, char const *argv[])
{
    int i = 2;
    float numero;
    float resultadoMultiplicacion;

    printf("Este programa sirve para mostrar la tabla de multplicar de un número del 2 al 10");
    printf("Ingresa el número: ");
    scanf("%f", &numero);

    while(i <= 10)
    {
        resultadoMultiplicacion = i * numero;
        printf("%d x %g = %g\n", i, numero, resultadoMultiplicacion);
        i++;
    }
    return 0;
}
